<?php
/*
 * Template Name: At a glance Agenda v2
 * Description: Speakers template page.
 */

get_header(); ?>

<div class="c grp">
	<div class="c-12">
		<div class="c-8 nl">

	
			<div class="tab-container">
                <!--<p style="text-align: center; margin-top: 20px;"><a class="button" title="Online registration" href="http://www.probiotaevent.com/register/">Register now</a></p>-->
                <h1 style="margin:0 0 20px 10px;">At a glance agenda</h1>
				<ul class="tabs">
					<li><a class="active" data-tabcontent="tab1" >Day one</a></li>
					<li><a data-tabcontent="tab2">Day two</a></li>
					<li><a data-tabcontent="tab3">Day three</a></li>
				</ul>
				
				<div id="tab1" class="tab-content">
					<p class="am-pm">AFTERNOON - 2 February 2016</p>
					<ul class="event-detail">
						<li><span class="time">14:00</span><p><a href="http://www.probiotaevent.com/first-focus-workshop/" title="First Focus Workshop" target="_blank"><strong>First Focus Workshop</a> (optional) <br />Prebiotics – a return to true science</strong><br />Recent thinking on prebiotics has been coloured by concerns about the commercial demands imposed within a complex regulatory environment. In this free-thinking, free-wheeling workshop we’ll cut free from all regulatory restraints to return to the true science that’s driving innovation and demonstrable benefits of prebiotics. Leading researchers will consider current thinking, identify gaps in our knowledge and prioritise the scientific work needed to fill them. Each presenter will address these issues in a specific area of prebiotic research.</p></li>
                    </ul>
                    <p class="am-pm">EVENING</p>
					<ul class="event-detail">
						<li><span class="time">18:00</span><p><a href="http://www.probiotaevent.com/abstracts/" title=Scientific Frontiers poster session" target="_blank"><strong>Scientific Frontiers poster session</a> and networking drinks</strong><br>The Scientific Frontiers poster session will feature posters presenting the latest state-of-the-art developments related to all aspects of prebiotic, probiotic and microbiome science relevant to health, wellbeing, consumers and industry. Posters will be judged during this session by the Scientific Committee and the winning posters presenters will be invited to present their work to the Probiota delegates on Day Three.</p></li>
					</ul>
				</div>
				<div id="tab2" class="tab-content">
					<p class="am-pm">MORNING - 3 February 2016</p>
                    <ul class="event-detail">
						<li><span class="time">09:00</span><p><strong>Welcome from the Chair and scene setting</strong><br><em>Annie-Rose Harrison-Dunn, Reporter, NutraIngredients.com</em></p></li>
						<li><span class="time">09:10</span><p><a href="http://www.probiotaevent.com/ewa-hudson/" title="Shifting markets and emerging demographics – the dynamics of today’s global probiotics market" target="_blank"><strong>Shifting markets and emerging demographics – the dynamics of today’s global probiotics market</strong></a><br><em><a href="http://www.probiotaevent.com/ewa-hudson/" title="Ewa Hudson" target="_blank">Ewa Hudson,</a> Global Head of Health and Wellness Research, Euromonitor</em></p></li>
						<li><span class="time">09:40</span><p><a href="http://www.probiotaevent.com/dr-yolanda-sanz/" title="EFSA update on the scientific requirements for the substantiation of health claims related to immunity, pathogens and gastrointestinal functions" target="_blank"><strong>EFSA update on the scientific requirements for the substantiation of health claims related to immunity, pathogens and gastrointestinal functions</strong></a><br><em><a href="http://www.probiotaevent.com/dr-yolanda-sanz/" title="Dr Yolanda San" target="_blank">Dr Yolanda Sanz,</a> Panel on Dietetic Products, Nutrition and Allergies (NDA), European Food Safety Authority (EFSA) & Spanish Council for Scientific Research (IATA-CSIC)</em></p></li>
						<li><span class="time">10:10</span><p><a href="http://www.probiotaevent.com/carine-lambert/" title="Towards a new design for probiotic foods in Europe" target="_blank"><strong>Towards a new design for probiotic foods in Europe</strong></a><br><em><a href="http://www.probiotaevent.com/carine-lambert/" title="Carine Lambert" target="_blank">Carine Lambert,</a> Executive Director, IPA Europe (International Probiotics Association – Europe)</em></p></li>
                        <li><span class="time">10:40</span><p><strong>Refreshments</strong></p></li>
						<li><span class="time">11:10</span><p><a href="http://www.probiotaevent.com/esther-jimenez-quintana/" title="As pure as mothers’ milk?" target="_blank"><strong>As pure as mothers’ milk? Bacteria in human milk and its impact on infant and maternal health</strong></a><br><em><a href="http://www.probiotaevent.com/esther-jimenez-quintana/" title="Dr Esther Jiménez Quintana" target="_blank">Dr Esther Jiménez Quintana,</a> Assistant Lecturer in the Department of Nutrition, Food Science and Technology at the Complutense University of Madrid and Researcher at ProbiSearch</em></p></li>
						<li><span class="time">11:40</span><p><a href="http://www.probiotaevent.com/david-mills/" title="Beyond mother’s milk" target="_blank"><strong>Beyond mother’s milk – restoring the infant microbiome</strong></a><br><em><a href="http://www.probiotaevent.com/david-mills/" title="Prof David Mills" target="_blank">Prof David Mills,</a> Peter J. Shields Endowed Chair in Dairy Food Science, Department of Food Science and Technology, University of California, Davis</em></p></li>
					</ul>
					<p class="am-pm">AFTERNOON</p>
                    <ul class="event-detail">
						<li><span class="time">12:10</span><p><strong>Speed networking</strong><br>Extend your horizons with a series of four minute meetings with your fellow delegates. Introduce yourself to a new contact every time the klaxon sounds and find out if you’ve got mutual interests that would make a subsequent, more in-depth meeting worthwhile.</p></li>
						<li><span class="time">13:00</span><p><strong>Roundtable lunches – discuss the issues that matter most to you</strong><br>Each of our lunch tables will be hosted by an industry expert who will lead an informal discussion on an industry hot topic. Join the table that suits you best.</p></li>
						<li><span class="time">14:30</span><p><a href="http://www.probiotaevent.com/prof-ingvar-bjarnason/" title="Sure and certain ground. The need for clinical evidence in the probiotic market" target="_blank"><strong>Sure and certain ground. The need for clinical evidence in the probiotic market</strong></a><br><em><a href="http://www.probiotaevent.com/prof-ingvar-bjarnason/" title="Professor Ingvar Bjarnason" target="_blank">Professor Ingvar Bjarnason,</a> MD, MSc, FRCPAth, FRCP(Glasg), DSc, BUPA Cromwell Hospital and King’s College Hospital and <a href="http://www.probiotaevent.com/barry-smith/" title="Barry Smith" target="_blank">Barry Smith,</a> Founder and Chairman, Symprove</em></p></li>
						<li><span class="time">15:00</span><p><a href="http://www.probiotaevent.com/prof-remco-kort/" title="Improving health and wealth in East Africa" target="_blank"><strong>Improving health and wealth in East Africa: How a locally produced yoghurt drink is overcoming the challenge of bringing probiotics to rural Africa</strong></a><br><em><a href="http://www.probiotaevent.com/prof-remco-kort/" title="Prof Remco Kort" target="_blank">Prof Remco Kort,</a> Principle Scientist, TNO and Founder, Yoba for Life Foundation</em></p></li>
						<li><span class="time">15:30</span><p><strong>Refreshments</strong></p></li>
						<li><span class="time">16:00</span><p><strong><a href="http://www.probiotaevent.com/professor-alessandra-graziottin/" title="The microbiome and the menopause" target="_blank">The microbiome and the menopause</a></strong><br><em><a href="http://www.probiotaevent.com/professor-alessandra-graziottin/" title="Professor Alessandra Graziottin" target="_blank">Professor Alessandra Graziottin,</a> Director of the Alessandra Graziottin Foundation and Director of the Centre of Gynaecology and Medical Sexology, San Raffaele Resnati</em></p></li>
						<li><span class="time">16:30</span><p><strong><a href="http://www.probiotaevent.com/dr-ian-jeffery/" title="The role of the gut microbiota in age-related health decline" target="_blank">The role of the gut microbiota in age-related health decline</a></strong><br><em><a href="http://www.probiotaevent.com/dr-ian-jeffery/" title="Dr Ian Jeffery" target="_blank">Dr Ian Jeffery,</a> Principal Investigator, Department of Microbiology and the APC Microbiome Institute,University College Cork</em></p></li>
						<li><span class="time">17:00</span><p><strong><a href="http://www.probiotaevent.com/dr-scott-bultman/" title="An end to controversy?" target="_blank">An end to controversy? The fibre-microbiota-butyrate axis in tumour suppression</a></strong><br><em><a href="http://www.probiotaevent.com/dr-scott-bultman/" title="Dr Scott Bultman" target="_blank">Dr Scott Bultman,</a> Assistant Professor, Department of Genetics, University of North Carolina School of Medicine</em></p></li>
						<li><span class="time">17:30</span><p><strong>The Chairman of Probiota’s Scientific Committee, Niall Hyland, announces the winners of the year’s poster session</strong></p></li>
						<li><span class="time">17:35</span><p><strong>Closing remarks</strong><br><em>Annie-Rose Harrison-Dunn, Reporter, NutraIngredients.com</em></p></li>                   
					</ul>
					<p class="am-pm">EVENING</p>
                    <ul class="event-detail">
						<li><span class="time">19:00</span><p>Departures to Probiota reception and dinner</p></li>
					</ul>
				</div>
				
				<div id="tab3" class="tab-content">
					<p class="am-pm">MORNING - 4 February 2016</p>
                    <ul class="event-detail">
						<li><span class="time">09:00</span><p><strong>Chairman’s re-cap of Day 2 and welcome back</strong></p></li>
						<li><span class="time">09:05</span><p><strong><a href="http://www.probiotaevent.com/dr-pierre-burguiere/" title="Detection of strain specific probiotics" target="_blank">Detection of strain specific probiotics and evaluation of the gut-resistome in clinical faecal samples of healthy adults receiving antibiotic and multi-strain probiotic treatment</a></strong><br><em><a href="http://www.probiotaevent.com/dr-pierre-burguiere/" title="Dr Pierre Burguière" target="_blank">Dr Pierre Burguière,</a> Preclinical Research Program Manager, Lallemand Health Solutions Inc.</em></p></li>
						<li><span class="time">09:35</span><p><strong><a href="http://www.probiotaevent.com/dr-carine-blanchard/" title="Live and heat-treated probiotics differently modulate mRNA stabilization" target="_blank">Live and heat-treated probiotics differently modulate mRNA stabilization: a role for micro-RNAs</a></strong><br><em><a href="http://www.probiotaevent.com/dr-carine-blanchard/" title="Dr Carine Blanchard" target="_blank">Dr Carine Blanchard,</a> Senior Research Scientist, Nutrition and Health Research, Nestlé Research Center</em></p></li>
						<li><span class="time">10:05</span><p><strong><a href="http://www.probiotaevent.com/dr-marco-pane/" title="1H-NMR metabolomic analysis of the effects of probiotic administration on human metabolic phenotype" target="_blank"><sup>1</sup>H-NMR metabolomic analysis of the effects of probiotic administration on human metabolic phenotype</a></strong><br><em><a href="http://www.probiotaevent.com/dr-marco-pane/" title="Dr Marco Pane" target="_blank">Dr Marco Pane,</a>Product Development Specialist, Probiotical Healthcare</em></p></li>
						<li><span class="time">10:35</span><p><strong>Refreshments</strong></p></li>
						<li><span class="time">11:00</span><p><strong>SCIENTIFIC FRONTIERS</strong><br>Presentations from the winners of the Scientific Frontiers poster session will deliver the latest cutting edge developments in Pre and Probiotic sciene</p></li>
						<li><span class="time">11:45</span><p><strong><a href="http://www.probiotaevent.com/todd-beckman/" title="GoodBelly – Good business" target="_blank">GoodBelly – Good business: How NextFoods brought probiotics to the drinks aisle</a></strong><br><em><a href="http://www.probiotaevent.com/todd-beckman/" title="Todd Beck" target="_blank">Todd Beckman,</a> Founder &amp; COO, NextFoods</em></p></li>
                    </ul>
					<p class="am-pm">AFTERNOON</p>
                    <ul class="event-detail">
						<li><span class="time">12:15</span><p><strong><a href="http://www.probiotaevent.com/amanda-hamilton/" title="Sexy probiotics?" target="_blank">Sexy probiotics? Welcome to the next generation of ferment</a></strong><br><em><a href="http://www.probiotaevent.com/amanda-hamilton/" title="Amanda Hamilton" target="_blank">Amanda Hamilton,</a> Nutrition Director, Rhythm Health</em></p></li>
						<li><span class="time">12:45</span><p><strong><a href="http://www.probiotaevent.com/panel-discussion/" title="Opportunities for life?" target="_blank">Closing panel discussion: Opportunities for life?</a></strong></p></li>
						<li><span class="time">13:15</span><p><strong>Chairman’s closing remarks</strong></p></li>
						<li><span class="time">13:20</span><p><strong>Networking lunch</strong></p></li>
						<li><span class="time">14:30</span><p><strong>Departures</strong></p></li>
                    </ul>
				</div>
                <!--<p style="text-align: center; margin-top: 20px;"><a style="margin-top: 20px;" class="button" title="Online registration" href="http://www.probiotaevent.com/register/">Register now</a></p>-->
			</div>		
		
		</div>
		<div id="sponsors-sidebar" class="c-4 nr">
			<?php get_sidebar(sponsors); ?>
		</div>
	</div>
</div>

	<script type="text/javascript">
        $(function () {
			var tabs = $("body").find("[data-tabcontent]");
			$(tabs).each(function () {
                if(!$(this).hasClass("active"))
                    $("#" + $(this).attr("data-tabcontent")).hide();
			});
			$(tabs).each(function() {
			    $(this).click(function () {
			        $("#" + $(this).attr("data-tabcontent")).siblings("div").hide();
			        $("#" + $(this).attr("data-tabcontent")).show();
			        $(this).parent().siblings().children("a").removeClass("active");
			        $(this).addClass("active");
			    });
			});
		});
		
		
	</script><!-- -->

<?php get_footer(); ?>