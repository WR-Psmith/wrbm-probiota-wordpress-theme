<?php
/*
 * Template Name: TEST
 * Description: TEST
 */

get_header(); ?>

<div class="c grp">
	<div class="c-12">
		<div class="c-8 nl">

			<style type="text/css">
			
		.tab-container { 
			float: left; 
			width: 100%; 
			margin-top: 25px;
			margin-bottom: 60px;
		}
			.tabs { 
				float: left; 
				width: 100%; 
				text-transform: uppercase; 
				font-size: 22px;
			}
				.tabs li {
					float: left;
					list-style-type: none;
				}
					.tabs li a {
						background: #5FBF78;
						color: #FFF;
						color: rgba(0,0,0,.25);
						float: left;
						font-weight: bold;
						margin-right: 1px;
						min-width: 150px;
						padding: 10px 12px;
						text-decoration: none;
						text-align: center;
						cursor: pointer;
					}
						.tabs li a.active, .tabs li a:hover { background: #55AB6B; color: #FFF; }
						.tabs li:first-child a { border-top-left-radius: 6px; }
						.tabs li:last-child a { border-top-right-radius: 6px; }
				.tab-content {
					background: #FFF;
					float: left;
					padding: 20px 0 20px 0;
					width: 100%;
				}
					.tab-content h2, .tab-content p { margin-left: 10px; margin-right: 10px; }
			
			#inline-tab-one, #inline-tab-two, #inline-tab-three, #inline-tab-four {
				border-bottom: 2px solid #F48B1F;
				padding-bottom: 18px;
				margin-bottom: 25px;
			}
			.inline-tabs { 
				border-bottom: 2px solid #F48B1F;
				float: left; 
				margin-top: 30px;
				font-size: 14px;
				width: 100%; 
				text-transform: uppercase;
			}
				.inline-tabs li {
					float: left;
					list-style-type: none;
					text-align: center;
				}
					.inline-tabs li:first-child a { border-top-left-radius: 6px; } 
					.inline-tabs li:last-child a { border-top-right-radius: 6px; }
						.inline-tabs li a { 
							color: #FFF;
							color: rgba(255,255,255,0.45);
							cursor: pointer;
							background: #58585A;
							float: left;
							font-weight: bold;
							margin-right: 1px;
							min-width: 150px;
							padding: 10px 12px;
							text-decoration: none;
						}
						.inline-tabs li a.active {
							background: #F48B1F;
							color: #FFF;
						}
			p.am-pm {
				background: #58585A;
				border-radius: 26px;
				color: #FFF;
				display: inline-block;
				font-size: 14px;
				font-weight: bold;			
				margin-bottom: 15px;
				padding: 5px 15px;
				width: auto;
			}
			
			.event-detail { 
				float: left;
				font-size: 14px;
				list-style-type: none;
				margin: 0 0 0 0;
				width: 100%;
			}
				.event-detail li {
					float: left;
					margin-bottom: 8px;
					padding: 10px 0 0 0;
					width: 100%;
				}
					.event-detail li span { 
						background: #58585A;
						border-radius: 26px;
						color: #FFF;
						float: left;
						font-weight: bold;
						margin: -5px 10px 0 10px;
						padding: 5px;
					}
					#inline-tab-one .event-detail li span,
					#inline-tab-two .event-detail li span,
					#inline-tab-three .event-detail li span,
					#inline-tab-four .event-detail li span { 
						background: #F48B1F;
					}
					
		
		
	</style>
	
			<div class="tab-container">
	
				<h1 style="margin:0 0 20px 10px;">At a glance agenda</h1>
	
				<ul class="tabs">
					<li><a class="active" data-tabcontent="tab1" >Day one</a></li>
					<li><a data-tabcontent="tab2">Day two</a></li>
					<li><a data-tabcontent="tab3">Day three</a></li>
				</ul>
				
				<div id="tab1" class="tab-content">
					<p class="am-pm">ARRIVALS</p>
					<ul class="event-detail">
						<li><span class="time">18:00</span> Welcome Reception</li>
						<li><span class="time">19:00</span> Beyond taste: Drinking wine with all your senses</li>
						<li><span class="time">20:00</span> Dinner</li>
					</ul>
				</div>
				<div id="tab2" class="tab-content">
					<ul class="event-detail">
						<li><span class="time">08:00</span>DSM Breakfast briefing</li>
						<li><span class="time">09:00</span>Chairman's welcome and scene setting</li>
						<li><span class="time">09:10</span>The Future of Food:   Managing confusion, complexity and consumers that want to have it all</li>
						<li><span class="time">09:40</span>Let's stop drinking the kool aid. Why old categories need new thinking</li>
						<li><span class="time">10:10</span>Refreshments</li>
						<li><span class="time">10:40</span>The Big Debate</li>
						<li><span class="time">11:40</span>Speed Networking</li>
						<li><span class="time">12:40</span>Roundtable lunches</li>
					</ul>
					<ul class="inline-tabs">
						<li><a data-tabcontent="inline-tab-one" class="active">Stream 1<br/>Food</a></li>
						<li><a data-tabcontent="inline-tab-two">Stream 2<br/>Country Focus</a></li>
					</ul>
					<div id="inline-tab-one" class="tab-content">
						<ul class="event-detail">
							<li><span class="time">14:00</span>Should we be social?  The role of 'conversational marketing' in food manufacturing businesses</li>
							<li><span class="time">14:30</span>The Digital Disruptor: How ecommerce, omnichannel retail and mobile innovation are changing the grocery industry</li>
							<li><span class="time">15:00</span>Food as fashion – giving consumers what they want (before they know they want it)</li>
							<li><span class="time">15:30</span>Refreshments</li>
							<li><span class="time">16:00</span>New faces of food.  Changing aesthetics in a digital age</li>
						</ul>
					</div>
					<div id="inline-tab-two" class="tab-content"> 
						<ul class="event-detail">
							<li><span class="time">14:00</span>Matching Methuselah - understanding the link between nutrition and longevity</li>
							<li><span class="time">14:30</span>Protein, carbohydrate and clean sports nutrition – essential ingredients of an athlete’s diet</li>
							<li><span class="time">15:00</span>Plants, power and pleasure:  How PlantFusion turns plants into nutrient dense shakes that taste great and provide power for life </li>
							<li><span class="time">15:30</span>Refreshments</li>
							<li><span class="time">16:00</span>Supplements for emerging markets – bypass complexity for commercial advantage</li>
						</ul>
					</div>	

					<ul class="event-detail">
						<li><span class="time">16:40</span>Take a CHANCE. Why producing low-price nutritious food needn't be a business risk</li>
						<li><span class="time">17:10</span>Time to take the medicine:  Lessons in personalised nutrition from the pharmaceuticals industry</li>
						<li><span class="time">17:40</span>Chairman closing remarks</li>
						<li><span class="time">19:30</span>Food Vision champagne reception and dinner</li>
					</ul>
					
				</div>
				
				<div id="tab3" class="tab-content">
					<ul class="event-detail">
						<li><span class="time">08:00</span>BASF Breakfast briefing</li>
						<li><span class="time">09:00</span>Chairman's re-cap of Day 1 and welcome back</li>
						<li><span class="time">09:10</span>From down-under to all over:  How Swisse Wellness is taking on the world</li>
						<li><span class="time">09:40</span>The Marketing revolution is underway.  Why the fusion of data and content will determine who comes out on top</li>
						<li><span class="time">10:10</span>Clicks and bricks:  How online marketing can drive demand on the high street</li>
						<li><span class="time">10:40</span>Refreshments</li>
					</ul>
					<ul class="inline-tabs">
						<li><a data-tabcontent="inline-tab-three" class="active">Stream 1<br/>Food</a></li>
						<li><a data-tabcontent="inline-tab-four">Stream 2<br/>Nutrition</a></li>
					</ul>
					<div id="inline-tab-three" class="tab-content">
						<ul class="event-detail">
							<li><span class="time">11:00</span>What are they thinking?  How 4,000 consumers believe about 'healthy food' and how they shop for it</li>
						</ul>
					</div>
					<div id="inline-tab-four" class="tab-content"> 
						<ul class="event-detail">
							<li><span class="time">11:00</span>Nutrigenomics and economics:  The commercial realities of personalised nutrition</li>
						</ul>
					</div>	

					<ul class="event-detail">
						<li><span class="time">11:40</span>Beverage Panel</li>
						<li><span class="time">12:15</span>Making food without plant or animal:  Can ”note by note cooking”  feed the world (as a result of studies in molecular gastronomy)?</li>
						<li><span class="time">12:45</span>Editorial review</li>
						<li><span class="time">13:00</span>Mintel interactive session & lunch</li>
						<li><span class="time">14:30</span>Departures</li>
					</ul>
				</div>
				
				
			</div>		
		
		</div>
		<div id="sponsors-sidebar" class="c-4 nr">
			<?php get_sidebar(sponsors); ?>
		</div>
	</div>
</div>

	<script type="text/javascript">
        $(function () {
			var tabs = $("body").find("[data-tabcontent]");
			$(tabs).each(function () {
                if(!$(this).hasClass("active"))
                    $("#" + $(this).attr("data-tabcontent")).hide();
			});
			$(tabs).each(function() {
			    $(this).click(function () {
			        $("#" + $(this).attr("data-tabcontent")).siblings("div").hide();
			        $("#" + $(this).attr("data-tabcontent")).show();
			        $(this).parent().siblings().children("a").removeClass("active");
			        $(this).addClass("active");
			    });
			});
		});
		
		
	</script>

<?php get_footer(); ?>