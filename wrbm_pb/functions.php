<?php
add_action( 'after_setup_theme', 'blankslate_setup' );
function blankslate_setup()
{
load_theme_textdomain( 'blankslate', get_template_directory() . '/languages' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-thumbnails' );
global $content_width;
if ( ! isset( $content_width ) ) $content_width = 640;
register_nav_menus(
array( 'main-menu' => __( 'Main Menu', 'blankslate' ) )
);
}
// Register Scripts
function register_scripts() {
 
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js', false, '3.1.0', false );
    wp_enqueue_script( 'jquery' );
 
}
add_action( 'wp_enqueue_scripts', 'register_scripts' );
// Create widget area for Home Carousel
if ( function_exists('register_sidebar') )
  register_sidebar(array(
    'name' => 'Homepage Carousel',
    'before_widget' => '<div id="carousel-homepage" class="c grp">
	<div class="c-12 nl">',
    'after_widget' => '</div></div>',
  )
);
// END Home Carousel widget area
// Sidebar Widget Area
if ( function_exists('register_sidebar') )
  register_sidebar(array(
    'name' => 'Sponsors Sidebar',
    'before_title' => '<h4 class="sponsor-carousel-title">',
    'after_title' => '</h4>',
  )
);
// END Sidebar Widget Area
// Create widget area for Home sponsor carousel
if ( function_exists('register_sidebar') )
  register_sidebar(array(
    'name' => 'Homepage Sponsor Carousel',
    'before_widget' => '
	<div class="c grp">
		<div class="c-12">',
    'after_widget' => '</div>
	</div>',
	'before_title' => '<h3>',
    'after_title' => '</h3>',
  )
);
// END Home Carousel widget area
// Create widget area for Testimonial carousel
if ( function_exists('register_sidebar') )
  register_sidebar(array(
    'name' => 'Testimonial Carousel',
	'before_title' => '<h3>',
    'after_title' => '</h3>',
  )
);
// END Testimonial Carousel widget area
// Custom Category Page Function
class category_page_meta_box {

	public function __construct() {

		if ( is_admin() ) {
			add_action( 'load-post.php',     array( $this, 'init_metabox' ) );
			add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );
		}

	}

	public function init_metabox() {

		add_action( 'add_meta_boxes',        array( $this, 'add_metabox' )         );
		add_action( 'save_post',             array( $this, 'save_metabox' ), 10, 2 );

	}

	public function add_metabox() {
		// Conditional check on page template
		global $post;

    		if(!empty($post))
    		{
				$pageTemplate = get_post_meta($post->ID, '_wp_page_template', true);

				if($pageTemplate == 'category-grid-page.php' )
        	{
		add_meta_box(
			'CategoryPage',
			__( 'Page Category', 'text_domain' ),
			array( $this, 'render_metabox' ),
			'page',
			'advanced',
			'core'
		);

	}
			}
	}

	public function render_metabox( $post ) {

		// Retrieve an existing value from the database.
		$wrbm_postcategory = get_post_meta( $post->ID, 'wrbm_postcategory', true );

		// Set default values.
		if( empty( $wrbm_postcategory ) ) $wrbm_postcategory = '';

		// Form fields.
		echo '<table class="form-table">';

		echo '	<tr>';
		echo '		<th><label for="wrbm_postcategory" class="wrbm_postcategory_label">' . __( 'Post Category', 'text_domain' ) . '</label></th>';
		echo '		<td>';
		wp_dropdown_categories( array( 'id' => 'wrbm_postcategory', 'name' => 'wrbm_postcategory', 'class' => 'wrbm_postcategory_field', 'selected' => $wrbm_postcategory ) );
		echo '			<p class="description">' . __( 'For category pages, select a Post Category', 'text_domain' ) . '</p>';
		echo '		</td>';
		echo '	</tr>';

		echo '</table>';

	}

	public function save_metabox( $post_id, $post ) {

		// Sanitize user input.
		$wrbm_new_postcategory = isset( $_POST[ 'wrbm_postcategory' ] ) ? sanitize_text_field( $_POST[ 'wrbm_postcategory' ] ) : '';

		// Update the meta field in the database.
		update_post_meta( $post_id, 'wrbm_postcategory', $wrbm_new_postcategory );

	}

}

new category_page_meta_box;
//End custom page category

add_action( 'comment_form_before', 'blankslate_enqueue_comment_reply_script' );
function blankslate_enqueue_comment_reply_script()
{
if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
}
add_filter( 'the_title', 'blankslate_title' );
function blankslate_title( $title ) {
if ( $title == '' ) {
return '&rarr;';
} else {
return $title;
}
}
add_filter( 'wp_title', 'blankslate_filter_wp_title' );
function blankslate_filter_wp_title( $title )
{
return $title . esc_attr( get_bloginfo( 'name' ) );
}
add_action( 'widgets_init', 'blankslate_widgets_init' );
function blankslate_widgets_init()
{
register_sidebar( array (
'name' => __( 'Sidebar Widget Area', 'blankslate' ),
'id' => 'primary-widget-area',
'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
'after_widget' => "</li>",
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );
}
function blankslate_custom_pings( $comment )
{
$GLOBALS['comment'] = $comment;
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
<?php 
}
add_filter( 'get_comments_number', 'blankslate_comments_number' );
function blankslate_comments_number( $count )
{
if ( !is_admin() ) {
global $id;
$comments_by_type = &separate_comments( get_comments( 'status=approve&post_id=' . $id ) );
return count( $comments_by_type['comment'] );
} else {
return $count;
}
}