<?php
/*
 * Template Name: Scientific Speakers
 * Description: Scientific Speakers template page.
 */

get_header(); ?>

<div class="c grp">

	<div id="section-head" class="c-12 white-box">

	<!-- Page Post -->

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
				<?php the_content(); ?>
				
				<div class="entry-links"><?php wp_link_pages(); ?></div>

			</div>
			<?php endwhile; endif; ?>
	</div>
	<div class="c-12">

<!-- End of Page Post -->
		
<!-- Speaker Gallery -->

		<ul class="speaker-gallery">
			<?php
				global $post;
				$args = array( 'category' => 9, 'posts_per_page' => -1 );
				$myposts = get_posts( $args );
				foreach( $myposts as $post ) :  setup_postdata($post); ?>
   				<li><span style="display:none">Hello</span>
				<?php the_post_thumbnail(); ?>
				<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<p><?php echo get_post(get_post_thumbnail_id())->post_excerpt; ?></p></li>
			<?php endforeach; wp_reset_postdata(); ?>
		</ul>

	</div>

<!-- End of Speaker Gallery -->

</div>

<?php get_footer(); ?>