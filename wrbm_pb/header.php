<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width" />
	
	<title><?php wp_title( ' | ', true, 'right' ); ?></title>
	
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/font-awesome.min.css">
	
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
	
	<!--[if gte IE 8]>
	<style>
		body { font-family: Arial, sans-serif !important; }
		#sponsor-footer { background: #F5F5F5 !important; }
		#copyright object { height: 70px; }
		.foogallery-owl-carousel a { display: block !important; }
		.menu a { padding: 13px 22px; }
		.ie-8-logo { display: block; }
		.logo.svg { display: none; }
		#testimonial-footer .owl-theme .owl-dots .active.owl-dot span { background: #2F603C; }
		#testimonial-footer .owl-theme .owl-dots .owl-dot span { background: #50A266; }
		#copyright object { display: none; }
		#testimonial-footer h3, #testimonial-footer p { color: #478F5A; }
	</style>
	<![endif]-->
	
	<!-- Font API's -->
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,700,300,600,400' rel='stylesheet' type='text/css'>
	
	<?php wp_head(); ?>
	
</head>

<body <?php body_class(); ?>>

<!–- Google Tag Manager -–>
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-M8QW8G"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M8QW8G');</script>
<!–- End Google Tag Manager -–>

<div class="c grp">
	<div id="sitehead" class="c-12">
		<div class="c-4 nl">
			<a class="ie-8-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="animate slideDown" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo_probiota.gif" alt="Probiota" title="Probiota" /></a>
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo svg"><object class="animate slideDown" type="image/svg+xml" data="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ProbiotaBarcelonaIPA2018Masterlogo.svg"></object></a>
		</div>
		<div id="global-btn" >
			<a href="http://www.probiotaamericas.com/">Go to Probiota Americas <i class="fa fa-globe"></i></a> 
		</div>
		<div id="global-btn-2">
			<a href="https://www.probiotaasia.com/">Go to Probiota Asia <i class="fa fa-globe"></i></a> 
		</div>
		<ul id="social">
			<li><a href="https://twitter.com/Probiota" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/twitter-link.png" width="32" height="32" /></a></li>
			<li><a href="https://www.linkedin.com/groups?home=&amp;gid=2866289&amp;trk=anet_ug_hm" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/linked-in.png" width="32" height="32" /></a></li>
		</ul>
	</div>
</div>
<div id="menu-c" class="c grp">
	<div id="menu" role="navigation" class="c-12 menu">
		<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
	</div>
</div>