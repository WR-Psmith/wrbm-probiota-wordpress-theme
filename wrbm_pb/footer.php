<!--
<div class="c grp">
	<div id="multi-column" class="c-12 white-box" style="margin-top: 26px;">

		<h2>Key Conference Points</h2>

		<div class="c-4 nl">
			<?php
			 $id = 677;
			 $p = get_page($id);
			 echo apply_filters('the_content', $p->post_content);
			 ?>
		</div>
		<div class="c-4">
			<?php
			 $id = 679;
			 $p = get_page($id);
			 echo apply_filters('the_content', $p->post_content);
			 ?>
		</div>
		<div class="c-4 nr">
			<?php
			 $id = 681;
			 $p = get_page($id);
			 echo apply_filters('the_content', $p->post_content);
			 ?>
		</div>
	</div>
</div>
-->
<div id="testimonial-footer" class="cfw">
	<div class="c grp">
		<div class="c-12">
			<div id="testimonial-container" class="c-8 nl">
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Testimonial Carousel") ) : ?>
				<?php endif;?>	
			</div>
			<div class="c-4 nr">
				<h3>Tweets</h3>
				<a class="twitter-timeline" data-widget-id="491504995934564353" href="https://twitter.com/Probiota">Tweets by @Probiota</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
			</div>
		</div>
	</div>
</div>
<div id="footer" class="cfw">
	<div class="c grp">
		<div class="c-12">
			<div class="c-6 nl">
				
				<h4>Probiota</h4>
			
				<div class="c-3 nl">
					<ul>
						<li><a href="/contact-us">Contact us</a></li>
						<li><a href="/about-us">About us</a></li>
					</ul>
				</div>
				<div class="c-3 nr">
					<ul>
						<!--<li><a href="#">News</a></li>-->
						<li><a href="/sponsors/become-a-sponsor">Sponsors</a></li>
					</ul>				
				</div>
				
			</div>
			
			<div class="c-2">
				<h4>Events</h4>
				<ul>
					<li><a href="http://www.probiotaamericas.com">Probiota Americas</a></li>
					<li><a href="http://www.probiotaasia.com">Probiota Asia</a></li>
				</ul>				
			</div>
			
			<div class="c-4 nr">
				<h4>Social</h4>
				<ul>
					<li><a href="https://twitter.com/Probiota">Follow us on Twitter</a></li>
					<li><a href="https://www.linkedin.com/groups?home=&gid=2866289&trk=anet_ug_hm">Join our LinkedIn Group</a></li>
				</ul>
			</div>
			
		</div>
	</div>
</div>
<?php wp_footer(); ?>

</body>
</html>