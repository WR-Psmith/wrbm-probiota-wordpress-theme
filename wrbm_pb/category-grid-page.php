<?php
/*
 * Template Name: Category Grid
 * Description: Template page for Grid display of posts in a specific category i.e. speakers.
 */

// Check if post is blank
function empty_content($str) {
    return trim(str_replace('&nbsp;','',strip_tags($str))) == '';
}

get_header(); ?>

<div class="c grp">

	<div id="section-head" class="c-12 white-box">

	<!-- Page Post. -->

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				
				<h1 class="entry-title"><?php the_title(); ?></h1> <?php edit_post_link(); ?>
			
				<?php the_content(); ?>
				
				<div class="entry-links"><?php wp_link_pages(); ?></div>

			</div>
			<?php endwhile; endif; ?>
	</div>
	<div class="c-12">

<!-- End of Page Post -->
		
<!-- Speaker Gallery -->

		<ul class="speaker-gallery">
			<?php
				$cat = get_post_meta( $post->ID, 'wrbm_postcategory', true );
				global $post;
				$args = array( 'category' => $cat, 'posts_per_page' => -1, 'published' => 1 );
				$myposts = get_posts( $args );
				foreach( $myposts as $post ) :  setup_postdata($post); ?>
   				<li>
				<?php the_post_thumbnail(); ?>
				<h2><a href="
				<?php if (empty_content($post->post_content)) {
                    // Create Link to Post
                    the_permalink();
                }
                else {
                        echo '#';
                    }
                ?>
                "><?php the_title(); ?></a></h2>
				<p><?php echo get_post(get_post_thumbnail_id())->post_excerpt; ?></p></li>
			<?php endforeach; wp_reset_postdata(); ?>
		</ul>

	</div>

<!-- End of Speaker Gallery -->

</div>

<?php get_footer(); ?>