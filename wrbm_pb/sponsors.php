<?php
/*
 * Template Name: Sponsors Page
 * Description: Sponsors template page.
 */

get_header(); ?>

<div class="c grp">

	<div id="section-head" class="c-12 white-box">

	<!-- Page Post -->

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
				<?php the_content(); ?>
				
				<div class="entry-links"><?php wp_link_pages(); ?></div>

			</div>
			<?php endwhile; endif; ?>
	</div>
	<div class="c-12">

<!-- End of Page Post -->
		
<!-- Sponsor Gallery // Removed 16/12/14 MB - to add back in.

		<ul class="sponsor-gallery">
			<?php
				global $post;
				$args = array( 'category' => 7, 'posts_per_page' => -1 );
				$myposts = get_posts( $args );
				foreach( $myposts as $post ) :  setup_postdata($post); ?>
   				<li>
				<?php the_post_thumbnail(); ?>
				<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<p><?php echo get_post(get_post_thumbnail_id())->post_excerpt; ?></p></li>
			<?php endforeach; wp_reset_postdata(); ?>
		</ul>
		
-->

	</div>

<!-- End of Sponsor Gallery -->

</div>

<?php get_footer(sponsorless); ?>