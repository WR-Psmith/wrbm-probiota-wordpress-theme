<?php get_header(); ?>
	<div class="c grp" style="margin-bottom: 52px;">
		<div class="c-12">
		<div class="post-inner">
			<div class="c-3 nl">
			<div class="post-thumb">
				<?php $name = get_post_meta($post->ID, 'ExternalURL', true);
					if( $name ) { ?>
						<a href="<?php echo $name; ?>" target="_blank"><?php the_post_thumbnail(); ?></a>
					<?php } else {
						the_post_thumbnail();
				} ?>
			</div>
			</div>
			<div class="c-9 nr">
				<?php if ( is_singular() ) { echo '<h1 class="entry-title">'; } else { echo '<h1 class="entry-title">'; } ?><?php the_title(); ?><?php if ( is_singular() ) { echo '</h1>'; } else { echo '</h1>'; } ?> <?php edit_post_link(); ?>
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'entry' ); ?>
				<?php if ( ! post_password_required() ) comments_template( '', true ); ?>
				<?php endwhile; endif; ?>
			</div>
		</div>
		</div>
	</div>
<?php get_footer(sponsorless); ?>