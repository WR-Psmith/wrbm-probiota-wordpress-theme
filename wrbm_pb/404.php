<?php get_header(); ?>

<div class="c grp">
	<div id="section-head" class="c-12 white-box">
		<h1><span>404</span></h1>
		<p>Sorry the page you are looking for cannot be found.</p>
		<p>Please visit the <a href="/">homepage</a> and try again.</p>
	</div>
</div>
	
<?php get_footer(); ?>