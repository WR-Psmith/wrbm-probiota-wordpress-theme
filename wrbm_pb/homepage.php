<?php
/*
 * Template Name: Homepage Template
 * Description: The homepage template.
 */

get_header(); ?>

<div id="carousel-homepage" class="c grp">
	<div class="c-12">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Homepage Carousel") ) : ?>
		<?php endif;?>
	</div>
</div>

<div class="c grp">

	<div class="c-12">
		
		<div id="intro-text" class="c-12 nl white-box">
			<div class="inner">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
				<?php the_content(); ?>
				<div class="entry-links"><?php wp_link_pages(); ?></div>

				<?php if ( ! post_password_required() ) comments_template( '', true ); ?>
				<?php endwhile; endif; ?>
			</div>	
		</div>

	</div>

</div>
<div id="sponsor-footer" class="cfw">
<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Homepage Sponsor Carousel") ) : ?>
		<?php endif;?>
</div>
<?php get_footer(); ?>