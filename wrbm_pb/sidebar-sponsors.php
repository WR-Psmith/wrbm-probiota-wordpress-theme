<ul id="sidebar-gallery">
	<?php
		$post_object = get_field('sidebar_gallery');
			if( $post_object ): 
				// override $post
				$post = $post_object;
				setup_postdata( $post );
					$gallery = get_the_ID();
						echo '<h4 class="sponsor-carousel-title">Gallery</h4>';
						echo do_shortcode("[foogallery id='" . $gallery . "']");
				wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
			endif;
	?>
</ul>
<ul id="sidebar">
	<?php dynamic_sidebar( 'Sponsors Sidebar' ); ?>
</ul>